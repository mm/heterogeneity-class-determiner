'''
    Initial version of this file has been edited. In current version Schema Extraction has been defiend as a function and can be
    called within the file or from another script.

    This file is imported in compare_collection.py script to extract schema for determining Heterogeneity Class
'''

import sys # Kommandozeilenargumente/Command Line Tools
import datetime # aktuelle Zeit beim Speichern eines Schemas/Time when saving a schema
import json, bson
from pymongo import MongoClient
from collections import OrderedDict
from math import floor



optional_prop_count=0

optionalProperties = []
'''
[DE] Funktion zum Bestimmen des JSON-Typs, Umwandlung von Python-Typ in JSON-Typ
[EN] Routine to determine a JSON type, converting a python type into a JSON type
'''
def jsonType(node):
    nodeType = type(node)
    if (nodeType in [str, bson.objectid.ObjectId]):
        jsonType = "string"
    elif (nodeType is int):
        jsonType = "integer"
    elif (nodeType is float):
        jsonType = "number"
    elif (nodeType is bool):
        jsonType = "boolean"
    elif (nodeType is list):
        jsonType = "array"
    elif (nodeType is dict):
        jsonType = "object"
    elif (node is None):
        jsonType = "null"
    else:
        jsonType = ""

    return jsonType


'''
[DE] ERSTER TEIL
Kernstück des Programms, schrittweises Einlesen der Struktur jedes JSON-Dokumentes und
Ablegen der Knoten und Kanten im Spanning Graph (rekursiv für die Dokumente)

[EN] First Part
Program Core, reading the structure of each JSON document step by step and
store the edges and nodes into the spanning graph (recursively for the documents)
'''
def extractSchema(node, nodeName, parentName, documentID, zaehler):
    zaehler= zaehler+1

    '''
    [DE] Typ bestimmen
    [EN] Determine the property type
    '''
    propertyType = jsonType(node)

    '''
    [DE] Speichern der Knoten- und Kanteninformationen
    [EN] Store information about nodes and edges
    '''
    storeNode(nodeName, propertyType, documentID)

    '''
    [DE] Speicherung der Edge (außer beim Rootknoten)
    [EN] Store the edge (omitting the root node)
    '''
    if parentName:
        storeEdge(nodeName, parentName, documentID)

    '''
    [DE] rekursive Aufrufe im Fall von object bzw. array
    [EN] Recursive calls in case of object or array
    '''
    parentName = nodeName

    # Array
    if propertyType == "array":
        arrayTypes = set()
        for i in range(len(node)):
            arrayType = jsonType(node[i])
            '''
            [DE] extractSchema wird aufgerufen bei array und object, und wenn ein
            primitiver Datentyp zum ersten Mal vorkommt

            [EN] extractSchema is called for an array and an object and when a
            primitive data type is present for the first time
            '''
            if arrayType not in arrayTypes.difference({"array", "object"}):
                zaehler= extractSchema(node[i], nodeName + "__a__" + arrayType, parentName, documentID, zaehler)
            arrayTypes.add(arrayType)

    #'''
    #[DE] Object: rekursiver Aufruf für jede Property des Objekts
    #[EN] Object: recursive call for each property of the object
    #'''
    elif propertyType == "object":
        for prop, value in node.items():
            zaehler= extractSchema(value, nodeName + "__o__" + prop, parentName, documentID, zaehler)
    return(zaehler)


'''
[DE] Funktion zum Speichern eines Nodes
[EN] Routine for storing a node
'''
def storeNode(name, propertyType, documentID):
    '''
    [DE] Test, ob Knoten schon vorhanden
    [EN] Check if node is already present
    '''
    node_is_already_present = False
    for index, node in enumerate(spanning_graph_nodes):
        if node["name"] == name:
            node_is_already_present = True
            break

    if node_is_already_present:
        '''
        [DE] Überprüfen, ob Typ übereinstimmt
        [EN] Check if types are equal
        '''
        storedType = node["propertyType"]
        if storedType != propertyType:
            '''
            [DE] schon verschiedene Typen vorhanden?
            [EN] Are different types already available?
            '''
            if type(storedType) is list:
                '''
                [DE] neuen Typ einfügen
                [EN] Store new type
                '''
                if propertyType not in storedType:
                    storedType.append(propertyType)
            else:
                '''
                [DE] Array aus altem und neuen Typ bilden
                [EN] Create an array of the old type and the new type
                '''
                storedType = [storedType, propertyType]

            '''
            [DE] Wert mit neuem Typ speichern
            [EN] Save the value with the new type
            '''
            spanning_graph_nodes[index]["propertyType"] = storedType
        '''
        [DE] documentID zur ID-Menge "occurrence" hinzufügen
        [EN] Add documentID to the ID set "occurrence"
        '''
        spanning_graph_nodes[index]["occurrence"].add(documentID)

    else:
        '''
        [DE] insert, falls noch nicht vorhanden
        [EN] Insert if not already present
        '''
        spanning_graph_nodes.append( { "name": name,
                                       "propertyType": propertyType,
                                       "occurrence": set([documentID])
                                     } )


'''
[DE] Funktion zum Speichern einer Edge
[EN] Routine to store an edge
'''
def storeEdge(node, parent, documentID):
    '''
    [DE] Test, ob Kante schon vorhanden
    [EN] Check if edge is already present
    '''
    edge_is_already_present = False
    for index, edge in enumerate(spanning_graph_edges):
        if edge["node"] == node and edge["parent"] == parent:
            edge_is_already_present = True
            break

    if edge_is_already_present:
        '''
        [DE] wenn vorhanden, dann Häufigkeit erhöhen (Menge von documentIDs, in denen die Kante vorkommt)
        [EN] If edge is already present then increase the frequency (set of doumentIDs which contain the edge)
        '''
        spanning_graph_edges[index]["occurrence"].add(documentID)
    else:
        '''
        [DE] wenn nicht vorhanden, speichern
        [EN] if not present, save edge
        '''
        spanning_graph_edges.append( { "node": node,
                                       "parent": parent,
                                       "occurrence": set([documentID])
                                     } )

'''
[DE] ZWEITER TEIL
Erstellung des Schemas aus den Informationen des Spanning Graph

[EN] SECOND PART
Creating the schema based on the spanning graph information
'''

def printSchema(parent, occurrence, collectionName):

    #####Added to keep track of optional properties
    global optional_prop_count
    global optionalProperties

    '''
    [DE] Aufbau des Schemas als OrderedDictionary (Python-dictionary, das Reihenfolge beibehält)
    [EN] Creating the schema as OrderedDictionary (Python dictionary which retains the order)
    '''
    schema = OrderedDict()

    '''
    [DE] Knoten im Spanning Graph finden
    [EN] Find a node in the spanning graph
    '''
    for parentNode in spanning_graph_nodes:
        if parentNode["name"] == parent:
            break

    '''
    [DE] Anzahl der Document IDs in occurrence
    [EN] Amount of Document IDs in "occurrence"
    '''

    node_occurrence = len(parentNode["occurrence"])

    propertyType = parentNode["propertyType"]
    '''
    [DE] Typ ins Schema schreiben
    [EN] Writing the type into the schema
    '''
    schema["type"] = propertyType

    '''
    [DE] Typ oder einer der Typen des Nodes ist Objekt (propertyType kann String ("object") oder Array ([..., "object", ...]) sein)
    [EN] Type or one of the node types is object (propertyType can be of String ("object") or array ([..., "object", ...]))
    '''
    if "object" in propertyType:
        '''
        [DE] alle ausgehenden Kanten des Objektes finden
        [EN] Find all outgoing edges of the object
        '''
        outgoing_edges = []
        for edge in spanning_graph_edges:
            if edge["parent"] == parent:
                outgoing_edges.append(edge)

        if len(outgoing_edges) > 0:
            '''
            [DE] Properties erstellen
            [EN] Create properties
            '''
            properties = {}
            '''
            [DE] required-Array erstellen
            [EN] Create array of required properties
            '''
            requiredProperties = []            
            '''
            [DE] jede ausgehende Kante ist eine Property
            [EN] Each outgoing node is a property
            '''
            for edge in outgoing_edges:
                node = edge["node"]
                '''
                [DE] Namenskonvention "__o__" beseitigen
                [EN] Remove the naming convention "__o__"
                '''
                pos = node.rfind("__o__")
                if pos != -1:
                    nodeName = node[pos+5:]
                '''
                [DE] Property ist required, wenn sie so oft vorkommt, wie die Parent-Property
                [EN] Property is required if it occurs as often as the parent property
                '''
                if len(edge["occurrence"]) == node_occurrence:
                    requiredProperties.append(nodeName)
                else:
                    ######Added to keep track of optional properties
                    optional_prop_count+=1
                    optionalProperties.append(nodeName)
                '''
                [DE] rekursiver Aufruf für jede Property
                [EN] Recursive call of each property
                '''
                properties[nodeName] = printSchema(node, node_occurrence, collectionName)
            '''
            [DE] Properties nach Key sortieren und ins Schema schreiben
            [EN] Sort properties by key and write them into the schema
            '''
            schema["properties"] = OrderedDict(sorted(properties.items(), key = lambda item: str.lower(item[0])))
            '''
            [DE] requiredProperties schreiben, wenn es welche gibt (sortiert)
            [EN] If there are any requiredProperties, write them in a sorted order
            '''
            if requiredProperties:
                schema["required"] = sorted(requiredProperties, key = str.lower)

    '''
    [DE] Typ oder einer der Typen des Nodes ist Array
    [EN] Type or one of the types of the node is an array
    '''
    if "array" in propertyType:
        '''
        [DE] alle ausgehenden Kanten des Arrays finden
        [EN] Find all outgoing edges of the array
        '''
        outgoing_edges = []
        for edge in spanning_graph_edges:
            if edge["parent"] == parent:
                outgoing_edges.append(edge)
        if len(outgoing_edges) == 1:
            '''
            [DE] Array hat nur einen Typ
            [EN] Array has only one type
            '''
            schema["items"] = printSchema(outgoing_edges[0]["node"], node_occurrence, collectionName)
        elif len(outgoing_edges) > 1:
            '''
            [DE] Array hat verschiedene Typen
            [EN] Array has different types
            '''
            schema["items"] = { "anyOf": [] }
            for edge in outgoing_edges:
                schema["items"]["anyOf"].append(printSchema(edge["node"], node_occurrence, collectionName))

    '''
    [DE] Description für Knoten hinzufügen
    [EN] Add description for nodes
    '''
    if parent != collectionName:
        #'''
        #[DE] prozentuales Vorkommen berechnen
        #[EN] calculate percentaged occurrence
        #'''
        occ_percentage = str( floor(node_occurrence*100/occurrence) ) + "%"
        global possibleOutliers
        if int(occ_percentage[:-1]) >= 95: possibleOutliers += occurrence - node_occurrence
        if int(occ_percentage[:-1]) <=  5: possibleOutliers += node_occurrence
        schema["description"] = "Occurrence: " + str(node_occurrence) + "/" + str(occurrence) + ", " + occ_percentage

    '''
    [DE] Schema zurückgeben
    [EN] Return schema
    '''
    return schema


'''
[DE] HAUPTPROGRAMM
[EN] MAIN PROGRAM
'''

possibleOutliers = 0

'''
[DE] Aufruf per extract.py "Name der Datenbank" "Name der Collection"
ohne Collection-Name werden alle Collections der gegebenen DB eingelesen
ohne Datenbank-Name werden alle Collections aller Datenbanken eingelesen

[EN] Program call: extract.py "Name of the Database" "Name of the Collection"
without specifiying a collection name, all collection of the database are read
without specifying a database name, all collections of all databases are read
'''

zaehler=0

#dbArg = "schemaextraction"
#collArg = None

if len(sys.argv) > 1:
    dbArg = sys.argv[1]
if len(sys.argv) > 2:
    collArg = sys.argv[2]
    

#client = MongoClient('localhost:27019',username='Aman', password='Aman',authSource='schemaextraction', authMechanism='SCRAM-SHA-256')

'''
[DE] DB extract für die Schemas
[EN] DB extract for the schemas (?)
'''
#extractDB = client["extract_pagebeat"]

#extractDB = client["extraction_result"]
#extractDB.schemas.remove({})

'''
[DE] Beginn-Zeit ermitteln
[EN] Get start time
'''
begin = datetime.datetime.now()
#print("Begin extract "+str(begin))
arrayTime = datetime.timedelta()

'''
[DE] Datenbestand initialisieren
[EN] initialize spanning graph nodes and edges
'''
spanning_graph_nodes = []
spanning_graph_edges = []



def schemaExtraction(client, dbArg, collArg, extractDB):
    
    global possibleOutliers
    '''
    [DE] Liste der zu untersuchenden DBs
    [EN] List of databases to be examined
    '''
    if dbArg:
        dbs = [dbArg]
    else:
        dbs = client.database_names()

    '''
    [DE] Behandlung jeder Mongo-Datenbank
    [EN] Treatment of each Mongo database
    '''
    for dbName in dbs:

        #print(dbName)
        
        '''
        [DE] Skript-eigene Datenbank überspringen
        [EN] Skip script's own databse
        '''
        #if dbName == "extract":
        #    continue
        '''
        [DE] Verbindung herstellen
        [EN] Establish a connection
        '''
        currentDB = client[dbName]

        '''
        [DE] Liste der zu untersuchenden Collections
        [EN] List of collections to be examined
        '''
        #if collArg:
        if (collArg=='All' or collArg=='ALL' or collArg=='all'):
            #collections = [collArg]
            collections = currentDB.collection_names()
        else:
            #collections = currentDB.collection_names()
            collections = [collArg]

        '''
        [DE] Behandlung jeder Collection
        [EN] Treatment of each collection
        '''
        #print(collections)
        schema_list=[]
        for collectionName in collections:
            zaehler=0
            '''
            [DE] System Collections überspringen (reservierter Namespace)
            [EN] Skip system collections (reserved namespace)
            '''
            if collectionName.startswith("system"):
                continue
            
            beginCollection = datetime.datetime.now()

            '''
            [DE] Verbindung herstellen
            [EN] Establish a connection
            '''
            currentCollection = currentDB[collectionName]
            currentCollection_count = currentCollection.count()
            '''
            [DE] alten Datenbestand löschen
            [EN] delete old data (reinitialize nodes and edges)
            '''
            spanning_graph_nodes = []
            spanning_graph_edges = []
            '''
            [DE] Beginn-Zeit nehmen
            [EN] Get start time
            '''
            beginLocal = datetime.datetime.now()

            '''
            [DE] alle Datensätze selektieren
            [EN] Select all datasets
            '''
            for jsonDocument in currentCollection.find():
                '''
                [DE] Schemaextraktion
                Nodes und Edges speichern, Aufruf der rekursiven Funktion

                [EN] Schema schema
                Save nodes and edges, call of the recursive routine
                '''
                zaehler= extractSchema(jsonDocument, collectionName, "", str(jsonDocument['_id']), zaehler)
            #print("########################################")
            #print(zaehler)

            '''
            [DE] Aus dem Spanning Graph: Schema auslesen und in JSON umwandeln (wenn Dokumente vorhanden)
            [EN] From the spanning graph: Read schema and convert to JSON (if documents are available)
            '''
            if currentCollection_count:
                '''
                [DE] OrderedDict behält Reihenfolge bei (bessere Lesbarkeit des Schemas)
                [EN] OrderedDict maintains the order (improved readability of the schema)
                '''
                schema = OrderedDict()
                '''
                [DE] Meta-Daten: Schema-Version, Titel und Beschreibung inkl. Erstellungszeit
                [EN] Meta data: schema version, title and description incl. time of creation
                '''
                schema["title"] = collectionName
                schema["description"] = "JSON Schema for collection " + collectionName + " of database " + dbName + ", created on " + str(datetime.datetime.now()) + "."
                schema["$schema"] = "http://json-schema.org/draft-04/schema#" # Schema entspricht Version 4 des JSON Schema-Draft/Schema equals version 4 of the JSON Schema draft
                '''
                [DE] Aufruf der Funktion printSchema, Hinzufügen der Eigenschaften
                [EN] Call of the printSchema routine, adding properties
                '''
                schema.update(printSchema(collectionName, currentCollection_count, collectionName))
                '''
                [DE] Ausgabe der Schemas
                [EN] Output of the schema
                '''
                #print()
                #print("######################################################################")
                #print("######################################################################")
                #print("######################################################################")
                #print()
                #print(json.dumps(schema, indent = 4))
                '''
                [DE] Ende-Zeit nehmen
                [EN] Get end time
                '''
                endLocal = datetime.datetime.now()
                diff= endLocal- beginLocal
                schema["time1"] = str(diff)


                ################My code
                schema_list.append(json.dumps(schema, indent = 4))

                '''
                [DE] Speichern in der MongoDB
                MongoDB akzeptiert keine $-Zeichen in Feldnamen

                [EN] Storing into MongoDB
                MongoDB does not accept $-signs in the field names
                '''
                del schema["$schema"]
                extractDB.schemas.insert(schema)

                endCollection = datetime.datetime.now()
                #print("Collection " + collectionName + ": " + str(endCollection-beginCollection))
                if possibleOutliers > 0:
                    print("Collection " + collectionName + ": " + str(possibleOutliers) + "/" + str(currentCollection_count))
                possibleOutliers = 0

                #print("Collection " + collectionName + ": " + str(diff))

        #print(optional_prop_count)
        return schema_list


#print(schemaExtraction(client,dbArg,'ALL')[3]
