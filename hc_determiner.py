'''
    To run this file user need to import below modules
    File extract_Outlier_version_zaehler_intl.py should also be imported for schema extraction purpose

    compare_collection defined as a function and called in the end of the script
    Currently printing all containers and values stored in it along with the final result.
    If not required can be commented
'''



import sys # Kommandozeilenargumente/Command Line Tools
import datetime # aktuelle Zeit beim Speichern eines Schemas/Time when saving a schema
import json, bson
from pymongo import MongoClient
import extract_Outlier_version_zaehler_intl ####Schema Extraction code


####Details of Client, database and collections, Pass collArg='All' if schema extraction for all collections
client = MongoClient('HOST:PORT',username='USER', password='PASS',authSource='DATABASE', authMechanism='SCRAM-SHA-256')
dbArg = "schemaextraction"
collArg = None
extractDB = client["extraction_result"]
extractDB.schemas.remove({})

def compare_collections(client, dbArg, extractDB):

    ####Current databse
    currentDB = client[dbArg]

    ####Get all collections
    collections = currentDB.collection_names()
    ####Dsiplay all the collections for user to see and select
    print('_______________________________________')
    print(collections)

    ####Get the SOURCE Collection, Collection 1, from user
    while(True):
    
        val=(input('Total collection in the database. Enter SOURCE collection (name/number), Collection 1, Exit to end the program : \t'))
        try:
            if(int(val) in range(len(collections)+1)):
                collection1=collections[int(val)-1]
                break
            else:
                print('Wrong name/Out of range value entered \t', val)
                
        except ValueError:
            if (val in collections):
                collection1=val
                break
            elif (val=='Exit' or val=='EXIT' or val=='exit'):
                print('Exit entered to end the program')
                sys.exit(0)
            else:
                print('Wrong name/Out of range value entered \t', val)
                
            
    print('Selected SOURCE collection, Collection 1, is  : \t', collection1)
    print('_______________________________________')

    ####Schema Extraction for SOURCE collection, Collection 1
    extract_Outlier_version_zaehler_intl.schemaExtraction(client,dbArg,collection1,extractDB)
    if(len(extract_Outlier_version_zaehler_intl.optionalProperties)):
        print('______________________________________________________')
        ####Print the Optional Properties, if any
        print('We have optional properties for SOURCE collection')
        print(extract_Outlier_version_zaehler_intl.optionalProperties)
        print('______________________________________________________')
    else:
        pass
    '''
    [DE] Verbindung herstellen
    [EN] Establish a connection with SOURCE Collection, Collection 1
    '''
    currentCollection1 = currentDB[collection1]
    currentCollection_count1 = currentCollection1.count()

    ####Input key for SOURCE COllection, Collection 1, from user
    ####Check whether key enetered exists in SOURCE COllection, Collection 1
    while(True):
        key1=(input('Enter the key for SOURCE collection, complete path : zB. Prop1.Prop2.Prop3...\t, , Exit to end the program : '))
        ####key1=key1.split('.') ####Currently not using
        if(currentCollection1.find({key1:{'$exists':'true'}}).count()):
            ####print(currentCollection1.find({key1:{'$exists':'true'}}).count()) ####Print the count of keys
            print('Key entered for SOURCE collection, Collection 1, is  : \t', key1)
            break
        elif (key1=='Exit' or key1=='EXIT' or key1=='exit'):
            print('Exit entered to end the program')
            sys.exit(0)
        else:
            print('Wrong key entered for SOURCE collection, Collection 1, \t', key1)
            ####Enter the valid key


    ####Display all the collections for user to see and select
    print('_______________________________________')
    print(collections)
    
    ####Get the TARGET Collection, Collection 2, from user
    while(True):
        val=(input('Total collection in the database. Enter TARGET collection (name/number), Candidate 2, Exit to end the program : \t'))
        try:
            if(int(val) in range(len(collections)+1)):
                collection2=collections[int(val)-1]
                break
            else:
                print('Wrong name/Out of range value entered \t', val)
                
        except ValueError:
            if (val in collections):
                collection2=val
                break
            elif (val=='Exit' or val=='EXIT' or val=='exit'):
                sys.exit(0)
            else:
                print('Wrong name/Out of range value entered \t', val)


    print('Selected TARGET collection, Collection 2, is  : \t', collection2)
    print('_______________________________________')

    '''
    [DE] Verbindung herstellen
    [EN] Establish a connection with Collection 2
    '''
    currentCollection2 = currentDB[collection2]
    currentCollection_count2 = currentCollection2.count()

    ####Input key for TARGET Collection, Collection 2, from user
    ####Check whether key enetered exists in TARGET Collection, Collection 2
    while(True):
        key2=(input('Enter the key for TARGET collection, complete path : zB. Prop1.Prop2.Prop3...\t, Exit to end the program : '))
        ####key2=key2.split('.') ####Currently not being used
        if(currentCollection2.find({key2:{'$exists':'true'}}).count()):
            ####print(currentCollection2.find({key2:{'$exists':'true'}}).count()) ####Print the number of keys
            print('Key entered for TARGET collection, Collection 2, is  : \t', key2)
            break
        elif (key2=='Exit' or key2=='EXIT' or key2=='exit'):
            print('Exit entered to end the program')
            sys.exit(0)
        else:
            print('Wrong key entered for TARGET collection, Collection 2, \t', key2)
            ####Enter the valid key
   


    ##########################################################################################
    #########################Comparing two collection#####################################
    ##########################################################################################

    ####Containers to store values after comparing between both the collections, SOURCE Collection & TARGET Collection
    only_in_collection1,only_in_collection2,one_2_one,one_2_many,many_2_one,many_2_many=[],[],[],[],[],[]


    ####Find always return mongo cursor object which can be read in for loop element by element ####INFO

    ####Get all distinct values for key entered by user
    for counter in currentCollection1.distinct(key1):
        ####Count the occurrence of distince values in SOURCE Collection, Collection 1
        val_count1=currentCollection1.find({key1:counter}).count()
        if (val_count1 ==1):
            ####If the occurence of value is 1 in SOURCE Collection, Collection 1, then match with Collection 2
            val_count2=currentCollection2.find({key2:counter}).count()
            if (val_count2==0):
                ####No such value found in TARGET Collection, Collection 2
                for value in currentCollection1.find({key1:counter}):
                    only_in_collection1.append({'_id':value['_id'],key1:value[key1]})
                #only_in_collection1.append(counter)
                
                pass
            elif (val_count2==1):
                ####Exactly 1 such value ound in TARGET Collection, Collection 2
                for value in currentCollection1.find({key1:counter}):
                    one_2_one.append({'_id':value['_id'],key1:value[key1]})
                #one_2_one.append(counter)
                pass
            elif (val_count2>1):
                ####Many such values found in TARGET Collection, Collection 2
                for value in currentCollection1.find({key1:counter}):
                    one_2_many.append({'_id':value['_id'],key1:value[key1]})
                #one_2_many.append(counter)
                pass
            else:
                pass
        elif (val_count1>1):
            ###If the occurance of value is more than 1 in SOURCE Collection, Collection 1, then match with TARGET Collection, Collection 2
            val_count2=currentCollection2.find({key2:counter}).count()
            if (val_count2==0):
                ####No such value found in TARGET Collection, Collection 2
                for value in currentCollection1.find({key1:counter}):
                    only_in_collection1.append({'_id':value['_id'],key1:value[key1]})
                    break
                #only_in_collection1.append(counter)
                pass
            elif (val_count2==1):
                ####Exactly 1 such value ound in TARGET Collection, Collection 2
                for value in currentCollection1.find({key1:counter}):
                    many_2_one.append({'_id':value['_id'],key1:value[key1]})
                    break
                #many_2_one.append(counter)
                pass
            elif (val_count2>1):
                ####Many such values found in TARGET Collection, Collection 2
                for value in currentCollection1.find({key1:counter}):
                    many_2_many.append({'_id':value['_id'],key1:value[key1]})
                    break
                #many_2_many.append(counter)
                pass
            else:
                pass

    ########Similary iterating through all values in TARGET Collection, Collection 2, to find dangling touples availabe only in TARGET Collection, Collection 2
    ####Get all distinct values for key entered by user
    for counter in currentCollection2.distinct(key2):
        #######Count the occurrence of distince values in TARGET Collection, COllection 2
        val_count2=currentCollection2.find({key2:counter}).count()
        if (val_count2 ==1):
            ###If the occurance is value 1 in TARGET Collection, Collection 2, then match with SOURCE Collection, Collection 1
            val_count1=currentCollection1.find({key1:counter}).count()
            if (val_count1==0):
                ####No such value found in SOURCE Collection, Collection 1
                for value in currentCollection2.find({key2:counter}):
                    only_in_collection2.append({'_id':value['_id'],key2:value[key2]})
                #only_in_collection2.append(counter)
                pass
            else:
                pass
        elif (val_count2 >1):
            ###If the occurence of the value is more than 1 in TARGET COllection, Collection 2, then match with SOURCE Collection, Collection 1
            val_count1=currentCollection1.find({key1:counter}).count()
            if (val_count1==0):
                ####No such value found in SOURCE Collection, Collection 1
                for value in currentCollection2.find({key2:counter}):
                    only_in_collection2.append({'_id':value['_id'],key2:value[key2]})
                    break
                #only_in_collection2.append(counter)
                pass
            else:
                pass
        else:
            pass


    #####Scenario 1, when storing only key in containers
    #print('only_in_collection1',only_in_collection1)
    #print('one_2_one',one_2_one)
    #print('one_2_many',one_2_many)
    #print('many_2_one',many_2_one)
    #print('many_2_many',many_2_many)
    #print('only_in_collection2',only_in_collection2)

    ####Scenarion 2, when storing whole entity for key in containers, Using this to print the output... Contains _id and keys
    print('###########only_in_collection1######### \n')
    if(len(only_in_collection1)):
        for counter in only_in_collection1:
            print(counter)
    else:
        pass

    print('###########one_2_one######### \n')
    if(len(one_2_one)):
        for counter in one_2_one:
            print(counter)
    else:
        pass

    print('###########one_2_many######### \n')
    if(len(one_2_many)):
        for counter in one_2_many:
            print(counter)
    else:
        pass

    print('###########many_2_one######### \n')
    if(len(many_2_one)):
        for counter in many_2_one:
            print(counter)
    else:
        pass

    print('###########many_2_many######### \n')
    if(len(many_2_many)):
        for counter in many_2_many:
            print(counter)
    else:
        pass

    print('###########only_in_collection2######### \n')
    if(len(only_in_collection2)):
        for counter in only_in_collection2:
            print(counter)
    else:
        pass


    ####Defining HETEROGENEITY CLASS depending upon the values compared and stored in containers
    if (len(extract_Outlier_version_zaehler_intl.optionalProperties)):
        print('HETEROGENEITY CLASS 3')
    else:
        if(len(only_in_collection1) == 0 and len(one_2_many) == 0 and len(many_2_one) == 0 and len(many_2_many) == 0 and len(only_in_collection2) == 0):
            print('HETEROGENEITY CLASS 1')
        else:
            print('HETEROGENEITY CLASS 2')
            pass
            
####Function call to Compare Collections
compare_collections(client, dbArg, extractDB)
