# Heterogeneity Class Determiner

The HC Determiner determines the heterogeneity class for MongoDB NoSQL Collections and consists of two files.

hc_determiner.py is the main script and needs to be executed. All comparing and heterogeneity class determining logics are within this script.
Also connection details for mongoDB needs to be modified in this file.

extract_Outlier_version_zaehler_intl.py is used to extract schema.
No need to change anything in this file.
